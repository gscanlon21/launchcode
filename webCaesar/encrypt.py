'''
import os
from Crypto.Cipher import AES
# from Crypto.Hash import SHA256
from passlib.utils import pbkdf2
import secret


def encrypt(key, filename):
    chunksize = 64 * 1024
    outputFile = "e_" + filename
    filesize = str(os.path.getsize(filename)).zfill(16)

    IV = os.urandom(16)

    encryptor = AES.new(key, AES.MODE_CBC, IV)

    with open(filename, 'rb') as infile:
        with open(outputFile, 'wb') as outfile:
            outfile.write(filesize)
            outfile.write(IV)

            while True:
                chunk = infile.read(chunksize)

                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += ' ' * (16 - (len(chunk) % 16))

                outfile.write(encryptor.encrypt(chunk))


def decrypt(key, filename):
    chunksize = 64 * 1024
    outputFile = filename[2:]

    with open(filename, 'rb') as infile:
        filesize = long(infile.read(16))
        IV = infile.read(16)

        decryptor = AES.new(key, AES.MODE_CBC, IV)

        with open(outputFile, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)

                if len(chunk) == 0:
                    break

                outfile.write(decryptor.decrypt(chunk))
            outfile.truncate(filesize)


def getKey(password):
    hasher = pbkdf2.pbkdf2(b'%s' % (password), b'%s' % (secret.hashPass()),
                           100000, keylen=32, prf='hmac-sha256')
    return hasher


def mainEncrypt(filename, password):
    encrypt(getKey(password), filename)


def mainDecrypt(filename, password):
    decrypt(getKey(password), filename)


if __name__ == '__main__':
    Main()
'''
