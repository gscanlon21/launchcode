#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest


class Name:
    def __init__(self, fullname=None):
        if fullname is None:
            fullname = input("What's your full name? ")
        self.fullname = fullname

    def getInitials(self):
        fullnameList = self.fullname.split()
        return ''.join([name[:1].upper() for name in fullnameList])


class MyTest(unittest.TestCase):
    def test_me(self):
        fullName = Name('Ozzie Smith')
        self.assertEqual(fullName.getInitials(), 'OS')  # OK
        fullName = Name('Bonnie blair')
        self.assertEqual(fullName.getInitials(), 'BB')  # OK
        fullName = Name('George')
        self.assertEqual(fullName.getInitials(), 'G')  # OK
        fullName = Name('Daniel Day Lewis')
        self.assertEqual(fullName.getInitials(), 'DDL')  # OK
        print(Name().getInitials(), " <<< Is this correct?")  # OK # Asks for user input


if __name__ == '__main__':
    unittest.main()
    # Inititals().main()
