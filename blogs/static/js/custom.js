
window.setTimeout(function() {
  $("#flashmsg").fadeTo(500, 0).slideUp(500, function() {
    $(this).remove();
  });
}, 5000);

$('#main-dropdown').on('show.bs.dropdown', function () {
  window.setTimeout(function() {
    $('#main-encrypt').focus();
  }, 0);
})
