#!/usr/bin/python3
# -*- coding: utf-8 -*-

# from __future__ import absolute_import, division
from flask import Flask, flash, render_template, session
# import urllib2
from datetime import datetime, date
from camelcase import CamelCase
import time
from multiprocessing import Process, Lock, Event
import pytz  # $ pip install pytz
from tzlocal import get_localzone  # $ pip install tzlocal
import re
import unittest
import secret
from collections import deque


class Crypto:
    def __init__(self, string=None):
        self.string = string

    def checkInput(self, string=None, keyNum=None, keyString=None):
        if keyString is not None:
            try:
                # Checks if keyString is a plain number
                int(keyString)
                # Can also be ValueError
                return "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString"
            except Exception:
                if not isinstance(keyString, str):
                    return "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString"
                elif len(keyString) < 1:
                    return "# vingenere(string:str, keyString:str) # <class 'ValueError'> # Enter a valid keyString"
        elif keyNum is not None:
            try:
                if str(keyNum) == 'True' or str(keyNum) == 'False':
                    raise TypeError("keyNum cannot be True or False")
                keyNum = int(keyNum)
            except ValueError:
                return "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum"
            except TypeError:
                return "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum"
            if len(str(keyNum)) < 1:
                return "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum"
        elif string is not None:
            if string is None:
                string = input(u"Enter a string:\u0020")
            if not isinstance(string, str):
                return "# Enter a valid string"
            elif len(string) < 1:
                return "# Enter a valid string"

        # All Good!
        return True

    def XOR(self, keyString='a', decrypt=False):
        string = self.string

        # Checks for valid input
        if self.checkInput(None, None, keyString) is not True:
            return self.checkInput(None, None, keyString)
        if self.checkInput(string) is not True:
            return self.checkInput(string)

        if decrypt is True:
            keyString = keyString * 55
        else:
            while len(string) >= len(keyString):
                keyString += keyString

        byteString = ''.join('{0:08b}'.format(ord(x), 'b') for x in string)
        byteKeyString = ''.join('{0:08b}'.format(ord(x), 'b') for x in keyString)

        encryptedByteString = ''

        for bit, keyBit in zip(byteString, byteKeyString):
            encryptedBit = int(bit) ^ int(keyBit)
            encryptedByteString += str(encryptedBit)

        encryptedString = ''.join(chr(int(encryptedByteString[i * 8:i * 8 + 8], 2)) for i in range(len(encryptedByteString) // 8))

        self.string = encryptedString
        return encryptedString

    def decryptXOR(self, keyString=None):
        unencryptedString = self.XOR(keyString, True)
        print(unencryptedString)

    # VINGENERE
    # char from input string | char from key | rotation amount | result char
    # --------------------------------------------------------------------------
    # T                      | b             | 1               | U
    # h                      | o             | 14              | v
    # e                      | o             | 14              | s
    # (space)                | n/a           | n/a             | (space)
    # c                      | m             | 12              | o
    # r                      | b             | 1               | s
    # o                      | o             | 14              | c
    # w                      | o             | 14              | k
    # (and so on …)
    def vingenere(self, keyString='a', decrypt=False):
        if decrypt is True:
            encrypt, correctCase = -1, 26
        else:
            encrypt, correctCase = 1, 0

        string = self.string
        # Gets user input if arguments not already defined

        # Checks for valid input
        if self.checkInput(None, None, keyString) is not True:
            return self.checkInput(None, None, keyString)
        if self.checkInput(string) is not True:
            return self.checkInput(string)

        # Match every character in string with it's corresponding keyString character
        splitKeyString = []
        splitKeyStringOrig = deque(list(keyString))
        keyLength = len(splitKeyStringOrig)
        loop = 0
        # Generates equal length string and keyString
        for i, char in enumerate(string):
            if len(string) >= len(splitKeyString):
                ordChar = ord(char)
                # Upper or lower case letter
                if (65 <= ordChar < 91) or (97 <= ordChar < 123):
                    if loop == 1:
                        # Handle keyString positioning for special characters
                        splitKeyString[-1] = splitKeyStringOrig[i % keyLength]
                        # Handling of special character group ends
                        loop = 0
                    else:
                        splitKeyString.append(splitKeyStringOrig[i % keyLength])
                # Handle special character group (begins/continues)
                else:
                    splitKeyString.append('f')  # 'f' for filler
                    # Next character
                    if len(string) > i + 1:
                        ordChar = ord(string[i + 1])
                    else:
                        # Next character is no more
                        break
                    # Special character grouping ends
                    if (65 <= ordChar < 91) or (97 <= ordChar < 123):
                        splitKeyString.append(splitKeyStringOrig[i % keyLength])
                        # Handle keyString positioning for special characters

                        splitKeyStringOrig.rotate(1)
                        loop = 1
                    # Two or more special characters in a row
                    else:
                        # Handle keyString positioning for special characters
                        splitKeyStringOrig.rotate(1)

        # Generate mildly encrypted returnString
        returnString = ''
        for char, keyChar in zip(string, splitKeyString):
            # Translate character into a Unicode code point
            ordChar = ord(char)
            # Adjust keyNum accordingly to correctly match uppercase keyString characters
            if keyChar.isupper():
                keyNum = abs(ord(keyChar.upper()) - 65)
            # Adjust keyNum accordingly to correctly match lowercase keyString characters
            else:
                keyNum = abs(ord(keyChar) - 97)
            # Upper case letter
            if 65 <= ordChar < 91:
                # Generate Unicode code point for new character
                newOrd = ordChar + correctCase + keyNum * encrypt
                while newOrd >= 91:
                    newOrd = newOrd - 26
            # Lower case letter
            elif 97 <= ordChar < 123:
                # Generate Unicode code point for new character
                newOrd = ordChar + correctCase + keyNum * encrypt
                while newOrd >= 123:
                    newOrd = newOrd - 26
            # Special character
            else:
                # Generate Unicode code point for new character
                newOrd = ord(char)

            # Translate Unicode code point back into a character
            newChar = chr(newOrd)
            returnString += newChar

        self.string = returnString
        return returnString

    def decryptVingenere(self, keyString=None):
        decryptedString = self.vingenere(keyString, True)
        print(decryptedString)

    # CAESAR
    # char | rot | Return value
    # --------------------------------------------------------------------------
    # a    | 13  | n
    # a    | 14  | o
    # a    | 0   | a
    # c    | 26  | c
    # c    | 27  | d
    # A    | 13  | N
    # z    | 1   | a
    # Z    | 2   | B
    # z    | 37  | k
    # !    | 37  | !
    # 6    | 13  | 6
    def caesar(self, keyNum=0):
        string = self.string
        # Gets user input if arguments not already defined

        # Checks for valid input
        if self.checkInput(None, keyNum) is not True:
            return self.checkInput(None, keyNum, None)
        if self.checkInput(string) is not True:
            return self.checkInput(string)

        # Rotate the letters in the string keyNum places, 'a'+25 => 'z'
        returnString = ''
        for char in string:
            # Translate character into a Unicode code point
            ordChar = ord(char)
            # Uppercase letter
            if (65 <= ordChar < 91):
                # Unicode code point for uppercase `a`
                baseOrd = 65
                # Handles negative keyNums
                if keyNum < 0:
                    # Generate Unicode code point for new character
                    # `+13` from baseOrd % -26 == -13
                    newOrd = ((ordChar + keyNum + 13) % 26) + (baseOrd)
                # Handles positive keyNums
                else:
                    # Generate Unicode code point for new character
                    # `-13` from baseOrd % 26 == 13
                    newOrd = ((ordChar + keyNum - 13) % 26) + (baseOrd)
            # Lowercase letter
            elif (97 <= ordChar < 123):
                # Unicode code point for lowercase `a`
                baseOrd = 97
                # Handles negative keyNums
                if keyNum < 0:
                    # Generate Unicode code point for new character
                    # `+7` from baseOrd % -26 == -7
                    newOrd = ((ordChar + keyNum + 7) % 26) + (baseOrd)
                # Handles positive keyNums
                else:
                    # Generate Unicode code point for new character
                    # `-19` from baseOrd % 26 == 19
                    newOrd = ((ordChar + keyNum - 19) % 26) + (baseOrd)
            # Special character
            else:
                # Generate Unicode code point for new character
                newOrd = ord(char)

            # Translate Unicode code point back into a character
            newChar = chr(newOrd)
            returnString += newChar

        self.string = returnString
        return returnString

    def decryptCaesar(self):
        # Rotate the letters in the string keyNum places, 'a'+25 => 'z'
        returnList = []
        for keyNum in range(26):
            possibleDecryptedString = self.caesar(1)
            returnList.append(possibleDecryptedString)
        return returnList


class DefaultTest(unittest.TestCase):
    # Testing
    class TestVingenere:
        def __init__(self):
            # VINGENERE # def vingenere(self,string=None,keyString=None):
            self.aE = DefaultTest().assertEqual
            self.testVingenere()

        def testVingenere(self):
            self.testVingenereExpected()
            self.testVingenereBadInput()
            # self.testVingenereUserInput()

        def testVingenereExpected(self):
            # Vingenere expected input
            C = Crypto('?apples?')
            self.aE(C.vingenere('B'), '?bqqmft?')  # OK
            C = Crypto('ap_ples')
            self.aE(C.vingenere('A'), 'ap_ples')  # OK
            C = Crypto('apple**s')
            self.aE(C.vingenere('AB'), 'aqpme**t')  # OK
            C = Crypto('apples!')
            self.aE(C.vingenere('Z'), 'zookdr!')  # OK
            C = Crypto('The crow flies at midnight!')
            self.aE(C.vingenere('BooM'),
                    'Uvs osck rmwse bh auebwsih!')  # OK
            # Decrypt Vingenere
            print("\nDecrypting Vingenere:")
            C.decryptVingenere('BooM')
            # VINGENERE capital
            C = Crypto('APP LES')
            self.aE(C.vingenere('b'), 'BQQ MFT')  # OK
            C = Crypto(' APPLES ')
            self.aE(C.vingenere('a'), ' APPLES ')  # OK
            C = Crypto('APP  LES')
            self.aE(C.vingenere('ab'), 'AQP  MET')  # OK
            C = Crypto('APP LES')
            self.aE(C.vingenere('z'), 'ZOO KDR')  # OK
            C = Crypto('tHE CROW FLIES AT MIDNIGHT?')
            self.aE(C.vingenere('bOOm'),
                    'uVS OSCK RMWSE BH AUEBWSIH?')  # OK

        def testVingenereUserInput(self):
            # User Input Vingenere
            print("\nUser Input:\nTesting Vingenere (string:str, keyString:str)\n")
            print(Crypto(input('Enter a string to encrypt: ')).vingenere(
                input('Enter a rotation keyword: ')), end='')  # Asks for user input
            _input = input(
                u"\u0009 <<< Is this correct? (y/n)\n\u0009\u0009NOTE: 'a' rotates 0, not 1\n").lower()
            if _input in ['y', 'yes']:
                print("Passed\n")
            elif _input in ['n', 'no']:
                raise AssertionError(
                    "Test Failed at user input for vingenere(string:str, keyString:str)")
            else:
                print("Skipped\n")

        def testVingenereBadInput(self):
            # BAD INPUT VINGENERE
            C = Crypto('')
            self.aE(C.vingenere('ok'), "# Enter a valid string")  # OK
            C = Crypto(0)
            self.aE(C.vingenere('ok'), "# Enter a valid string")  # OK
            C = Crypto([])
            self.aE(C.vingenere('ok'), "# Enter a valid string")  # OK
            C = Crypto({})
            self.aE(C.vingenere('ok'), "# Enter a valid string")  # OK
            C = Crypto(True)
            self.aE(C.vingenere('ok'), "# Enter a valid string")  # OK
            C = Crypto('ok')
            self.aE(C.vingenere(
                []), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                {}), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                0), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                ''), "# vingenere(string:str, keyString:str) # <class 'ValueError'> # Enter a valid keyString")  # OK
            self.aE(C.vingenere(
                True), "# vingenere(string:str, keyString:str) # <class 'TypeError'> # Enter a valid keyString")  # OK
            print("\nVingenere Passed\n")

    class TestCaesar:
        def __init__(self):
            # CAESAR # def caesar(self,string=None,keyNum=None):
            self.aE = DefaultTest().assertEqual
            self.testCaesar()

        def testCaesar(self):
            self.testCaesarExpected()
            self.testCaesarBadInput()
            # self.testCaesarUserInput()

        def testCaesarExpected(self):
            # CAESAR Expected Inputs
            C = Crypto('a')
            self.aE(C.caesar(1), 'b')  # OK
            # self.aE(C.caesar(-1), 'z')  # OK
            # self.aE(C.caesar(-27), 'z')  # OK
            # self.aE(C.caesar(27), 'b')  # OK
            # self.aE(C.caesar(0), 'a')  # OK
            # self.aE(C.caesar(1), 'b')  # OK
            C = Crypto('apple')
            self.aE(C.caesar(26), 'apple')  # OK
            C = Crypto('Hello, World!')
            self.aE(C.caesar(5), 'Mjqqt, Btwqi!')  # OK
            # Decrypt Caesar
            print("\nDecrypting Caesar:")
            C.decryptCaesar()
            # CAESAR capital
            C = Crypto('A')
            self.aE(C.caesar(1), 'B')  # OK
            # self.aE(C.caesar(27), 'B')  # OK
            # self.aE(C.caesar(-27), 'Z')  # OK
            # self.aE(C.caesar(-1), 'Z')  # OK
            # self.aE(C.caesar(0), 'A')  # OK
            # self.aE(C.caesar(1), 'B')  # OK
            C = Crypto('APPLE')
            self.aE(C.caesar(26), 'APPLE')  # OK
            C = Crypto('W')
            self.aE(C.caesar(5), 'B')  # OK

        def testCaesarUserInput(self):
            # CAESAR user input
            print("\n\nUser Input:\nTesting caesar(string:str, keyNum:int)\n")
            print(Crypto(input('Enter a string to encrypt: ')).caesar(
                input('Enter rotation amount: ')), end='')  # OK # Asks for user input
            _input = input(u"\u0009 <<< Is this correct? (y/n)\n").lower()
            if _input in ['y', 'yes']:
                print("Passed\n")
            elif _input in ['n', 'no']:
                raise AssertionError("Test Failed at user input for caesar(string:str, keyNum:int)")
            else:
                print("Skipped\n")

        def testCaesarBadInput(self):
            # BAD INPUT VINGENERE
            C = Crypto('')
            self.aE(C.caesar(1), "# Enter a valid string")  # OK
            C = Crypto(0)
            self.aE(C.caesar(1), "# Enter a valid string")  # OK
            C = Crypto(True)
            self.aE(C.caesar(1), "# Enter a valid string")  # OK
            C = Crypto({})
            self.aE(C.caesar(1), "# Enter a valid string")  # OK
            C = Crypto([])
            self.aE(C.caesar(1), "# Enter a valid string")  # OK
            C = Crypto('ok')
            self.aE(C.caesar(''), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar('hello'), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar(True), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")   # OK
            self.aE(C.caesar([]), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar({}), "# caesar(string:str, keyNum:int) # <class 'TypeError'> # Enter a valid keyNum")  # OK
            self.aE(C.caesar('oh'), "# caesar(string:str, keyNum:int) # <class 'ValueError'> # Enter a valid keyNum")  # OK
            print("\nCaesar Passed\n")

    class TestXOR:
        def __init__(self):
            # XOR # def vingenere(self,string=None,keyString=None):
            self.aE = DefaultTest().assertEqual
            self.testXOR()

        def testXOR(self):
            C = Crypto('Secret Message: ...This is the secret!')
            print(C.XOR('Hello World!'))
            C.decryptXOR('Hello World!')

    def testAll(self):
        self.TestCaesar()
        self.TestVingenere()
        self.TestXOR()
        print("\nAll Passed", end='')


if __name__ == '__main__':
    unittest.main()
    # Crypto().caesar()


'''
def getformatteddate(mydate):
    # global today
    # global todayprettier
    punctuationList = ['/', ',', '-', ':', '.']

    ts = time.time()
    try:
        timezone = session['timezone']
    except Exception as e:
        timezone = 'US/Central'
    central = pytz.timezone(timezone)
    utc_now, now = datetime.utcfromtimestamp(ts), datetime.fromtimestamp(ts)
    local_now = now.replace(tzinfo=pytz.utc).astimezone(central)  # utc -> local
    # assert local_now.replace(tzinfo=None) == now
    today = local_now
    todaypretty = str(today)[:-13]
    todayprettier = today.date()

    if mydate[:3].lower() == 'tom':
        wordLength = len(mydate)
        mydate1 = mydate[wordLength:]
        mydate = "{:%m %d %Y}".format(today)
        mydate = str(mydate).split(' ')
        day = mydate[1]
        day = int(day)
        day += 1
        day = str(day)
        mydate[1] = day
        mydate = ' '.join(mydate)
        mydate += mydate1

    elif mydate[:3].lower() == 'tod':
        wordLength = len(mydate)
        mydate1 = mydate[wordLength:]
        mydate = "{:%m %d %Y}".format(today)
        mydate = str(mydate)
        mydate += mydate1

    for item in punctuationList:
        mydate = mydate.split(item)
        mydate = ' '.join(mydate)

    formats = ['%b %d %Y %H %M', '%B %d %Y %H %M', '%m %d %Y %H %M', '%m %d %y %H %M', '%B %d %y %H %M', '%b %d %y %H %M', '%b %d %Y', '%B %d %Y', '%m %d %Y', '%m %d %y', '%B %d %y', '%b %d %y', '%b %d %Y %H',
               '%B %d %Y %H', '%m %d %Y %H', '%m %d %y %H', '%B %d %y %H', '%b %d %y %H', '%b %d %Y %H %M %S', '%B %d %Y %H %M %S', '%m %d %Y %H %M %S', '%m %d %y %H %M %S', '%B %d %y %H %M %S', '%b %d %y %H %M %S']

    dateValue = 'Error'

    for fmt in formats:
        try:
            dateValue = datetime.strptime(mydate, fmt)
        except ValueError as e:
            pass

    if dateValue is None:
        return 'Error'
    else:
        return dateValue


def Mainy():
    global c
    global yest
    # global today
    global now

    c = CamelCase()

    local_tz = get_localzone()
    ts = time.time()
    utc_now, now = datetime.utcfromtimestamp(ts), datetime.fromtimestamp(ts)
    local_now = utc_now.replace(tzinfo=pytz.utc).astimezone(local_tz)  # utc -> local
    assert local_now.replace(tzinfo=None) == now
    today = now

    yest = 0

    global lock
    lock = Lock()
    global exit
    exit = Event()
    global proc

    # from main import runapp

    try:
        proc = Process(target=notification, args=())
        proc.daemon = True
        time.sleep(.1)
        proc.start()
        # proc.notification()
        # raw_input("\nEnter to terminate\n")
        # proc.terminate()
        # proc.join()
    except Exception as e:
        proc.terminate()
        proc.join()
'''
