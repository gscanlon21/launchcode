#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest


class TreeNode:
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.right = right
        self.left = left

    def __repr__(self):
        return ('<TreeNode value={} left={} right={}>'
                .format(
                    self.value,
                    None if not self.left else hex(id(self.left)),
                    None if not self.right else hex(id(self.right)),
                ))

    def insert(self, heapRoot, val):
        # Recursively insert val into the heap
        if heapRoot.value > val:
            if heapRoot.left is not None:
                self.insert(heapRoot.left, val)
            else:
                heapRoot.left = TreeNode(val)
        else:
            if heapRoot.right is not None:
                self.insert(heapRoot.left, val)
            else:
                heapRoot.right = TreeNode(val)

    def search(self, heapRoot, val):
        if heapRoot.value > val and heapRoot.left is not None:
            if heapRoot.left.value != val:
                self.search(heapRoot.left, val)
            else:
                return heapRoot.left
        elif heapRoot.right is not None:
            if heapRoot.right.value != val:
                self.search(heapRoot.left, val)
            else:
                return heapRoot.right
        return None

    def delete(self, heapRoot, val):
        if heapRoot.value > val and heapRoot.left is not None:
            if heapRoot.left.value != val:
                self.delete(heapRoot.left, val)
            else:
                heapRoot.left = None
        elif heapRoot.right is not None:
            if heapRoot.right.value != val:
                self.delete(heapRoot.left, val)
            else:
                heapRoot.right = None
        return None

    def toList(self, heapRoot):
        # Turn a heap into a flattened List
        left = self.toList(heapRoot.left) if heapRoot.left is not None else []
        right = self.toList(heapRoot.right) if heapRoot.right is not None else []
        return left + [heapRoot.value] + right


class DefaultTest(unittest.TestCase):
    # Testing
    def testMe(self):
        aE = self.assertEqual
        aIN = self.assertIsNone
        aR = self.assertRegex
        regex = '<\w*\s\w*=(\w|\d)*\s\w*=.*\s\w*=.*>'
        root = TreeNode(10)
        root.right = TreeNode(24)
        aR(root.__repr__(), regex)
        aR(root.right.__repr__(), regex)
        root.insert(root, 8)
        root.insert(root, 7)
        aR(root.left.__repr__(), regex)
        aR(root.left.left.__repr__(), regex)
        root.delete(root, 7)
        aR(root.left.__repr__(), regex)
        aIN(root.left.left)
        aE(root.toList(root).__repr__(), '[8, 10, 24]')
        aR(root.search(root, 8).__repr__(), regex)
        aIN(root.delete(root, 88))
        aIN(root.search(root, 88))


if __name__ == '__main__':
    unittest.main()
