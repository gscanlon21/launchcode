# TemperatureSensor.py
from collections import deque

# $ python3 lab3.py fahrenheit_small.txt
# Outputs:
# <TemperatureSensor temps=[] degree_unit=Celsius
# inputfile=fahrenheit_small.txt>
#
# <TemperatureSensor
# temps=deque(['-40.0', '0.0', '32.0', '50.0', '98.6', '212.0'])
# degree_unit=Fahrenheit inputfile=fahrenheit_small.txt>
#
# <TemperatureSensor
# temps=[-40.032000000000004, -17.792, 0.0, 10.008000000000001, 37.0296, 100.08000000000001]
# degree_unit=Celsius inputfile=fahrenheit_small.txt>


class TemperatureSensor:
    '''A class representing a hardware sensing device which works with
    temperature data. This device could be part of home heating system,
    a car engine, a portable weather station, etc.
    '''

    def __init__(self, temperatures=[], degree_unit='Celsius', inputfile=''):
        # MVM: Design question - should this class have a member
        # for the output file name?
        self.__temps = temperatures
        self.__degree_unit = degree_unit
        self.__inputfile = inputfile

    # Methods with a '_' at the beginning indicate that a
    # user probably shouldn't call these directly. That is,
    # it's okay to call them inside of other method definitions,
    # but possibly not a good idea to call them in a driver file like
    # lab3.py.
    def _fahrenheit2celsius(self):
        self.__temps = [(float(t) - 32) * 0.556 for t in self.__temps]
        self.__degree_unit = 'Celsius'

    def _celsius2fahrenheit(self):
        self.__temps = [1.8 * float(t) + 32 for t in self.__temps]
        self.__degree_unit = 'Fahrenheit'

    # There are two accessors. Would it make sense to
    # have mutators, as well?
    def get_temperatures(self):
        return self.__temps

    def get_degree_unit(self):
        return self.__degree_unit

    # These are method forms of code from Lab 2.
    def convert_temperatures(self):
        '''Converts the object's temperatures from Fahrenheit to Celsius,
        or vice versa.'''
        if self.__degree_unit == 'Fahrenheit':
            self._fahrenheit2celsius()
        elif self.__degree_unit == 'Celsius':
            self._celsius2fahrenheit()

    def load_data(self):
        '''Loads temperature data from inputfile.
        Assumes file format has degree type on first line and
        individual temperatures, one per line, for the rest of the file.
        '''
        with open(self.__inputfile, 'r') as f:
            data = deque(f.read().splitlines())

        tempType = data.popleft()

        self.__degree_unit = tempType
        self.__temps = data

    def save_data(self, filename='temperature_sensor_output.txt'):
        '''Writes temperature data to filename. Format is the degree
        type on the first line, then each of the temperatures on
        individual lines.
        '''
        with open(filename, 'w') as f:
            f.write('{0}\n'.format(self.__degree_unit))
            for temp in self.__temps:
                f.write('{0:.2f}\n'.format(temp))

    def get_stats(self):
        '''Returns a dictionary of the mininum, maximum, mean, and median
        temperatures.'''
        # what should happen if this gets called before any data
        # has been loaded?
        temps = self.__temps
        s = {}
        s['min'] = min(temps)
        s['max'] = max(temps)
        s['mean'] = sum(temps) / len(temps)

        if len(temps) % 2 == 1:
            s['median'] = temps[len(temps) // 2]
        else:
            s['median'] = (temps[len(temps) // 2] + temps[len(temps) // 2 - 1]) / 2

        return s

    def __repr__(self):
        #  __repr__ is a special method so that we can send an
        # object to print(). There is also __str__. __str__ is if
        # we wanted to print inside of a sentence, so it's usually
        # pretty brief. __repr__ is usually used to give a more
        # detailed description of the object. Take some creative liberties here.
        return ('<TemperatureSensor temps={} degree_unit={} inputfile={}>'
                .format(
                    self.__temps,
                    self.__degree_unit,
                    self.__inputfile
                ))


# Above this line there should be only things like
# import statments, class definitions, and function definitions.
# That is, no "live" code -- nothing which would print or get
# executed when imported into another file like lab3.py.
if __name__ == '__main__':
    # Common to have a trivial example of instantiating an
    # object from the class here.
    print('Class definition file being called directly.')
    print('Example instantiation:')
    testSensor = TemperatureSensor([-40, 0, 10, 50, 100])
    print(testSensor)
    print('Converting degrees.')
    testSensor.convert_temperatures()
    print(testSensor)
